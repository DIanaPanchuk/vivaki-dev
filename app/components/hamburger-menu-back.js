import Ember from 'ember';

export default Ember.Component.extend({
    eventBus: Ember.inject.service('event-bus'),
    classNames: ['hamburger', 'opened'],
    actions: {
        back: function() {
            history.back();
        }
    }
});
