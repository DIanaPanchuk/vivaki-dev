import Ember from 'ember';

export default Ember.Component.extend({
    eventBus: Ember.inject.service('event-bus'),
    classNames: ['modalWrap'],
    expose: function() {
        var exposedName = "comp-" + this.get('id');
        this.get('targetObject').set(exposedName, this);
    }.on('init'),
    _listen: function() {
        this.get('eventBus').on('toggleModal', this, 'modalToggle');
    }.on('init'),
    modalToggle: function() {
        this.$().toggleClass('visible');
    }
});