/* globals particlesJS */
import Ember from 'ember';

export default Ember.Component.extend({
    classNames: ['connectedParticles'],

    didInsertElement: function() {
        particlesJS.load(this.elementId, 'assets/particles-config.json', function() {
            console.log('particles.js config loaded');
        });
    }
});
