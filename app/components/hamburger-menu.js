import Ember from 'ember';

export default Ember.Component.extend({
    eventBus: Ember.inject.service('event-bus'),
    classNames: ['hamburger anim'],
    actions: {
        openModal: function(){
            var currentRoute = this._controller.get('currentPath');
            if (currentRoute == 'feedback' || currentRoute == "call-me" || currentRoute == "social") {
                history.back();
            } else if (Ember.$('#menu').hasClass('visible')) {
                this.get('eventBus').trigger('toggleModal');
                this.$().removeClass('opened');
                Ember.$('#logo').addClass('invisible');
                // Ember.$('.liquid-container').removeClass('hidden');
                Ember.$('#menu').removeClass('visible');
                Ember.$('#logo').removeClass('invisible');

                Ember.$('.liquid-container').removeClass('menuOpened');
                Ember.$('#menu').removeClass('open');
                $('body > div').removeClass('menuOpened');
            } else {
                this.get('eventBus').trigger('toggleModal');
                this.$().toggleClass('opened');
                // Ember.$('.liquid-container').toggleClass('hidden');
                Ember.$('#logo').toggleClass('invisible');

                Ember.$('.liquid-container').addClass('menuOpened');
                Ember.$('#menu').addClass('open');
                $('body > div').addClass('menuOpened');
            }
        }
    }
});
