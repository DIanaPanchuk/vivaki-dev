import Ember from 'ember';

export default Ember.Component.extend({
	didInsertElement: function() {
		var showIntructions = this.get('getCookie');
		if (showIntructions == "show") {
			Ember.$('.liquid-child, body').addClass('ovfHidden');
			// Ember.$('.main').addClass('invisible');
			Ember.$('.liquid-container').addClass('toFront');
			Ember.$('.sphere-wrap').removeClass('hidden');

			Ember.$('.sphere, .sphereCompany').addClass('sphereHide');
		}
	},
	classNames: ['modalWrap'],
	actions: {
		closeModal: function() {
			this.sendAction('closeModal');
		}
	}
});
