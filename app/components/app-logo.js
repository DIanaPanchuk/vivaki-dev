import Ember from 'ember';

export default Ember.Component.extend({
	didInsertElement: function() {
		if (Ember.$(document).width() >= 960) {
			$("html").niceScroll({
				cursorwidth: 0
			});
		}
	}
});
