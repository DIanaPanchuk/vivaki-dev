import Ember from 'ember';
import Validation from 'vivaki/mixins/validation';

export default Ember.Component.extend(Validation, {
	didInsertElement: function() {
		this.validate();
		Ember.$('.hamburger').addClass('opened');
	},
	actions: {
		submit: function() {
			this.sendAction('action', this.get('call'));
		},
		preferredTimeSelected: function(context) {
			var preferredTime = context.text;
			console.log(preferredTime);
			this.sendAction('preferredTimeSelected', preferredTime);
		}
	}
});
