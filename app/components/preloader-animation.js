import Ember from 'ember';

export default Ember.Component.extend({
	classNames: ['preloader'],
	didInsertElement: function() {
		var s = Snap("#shapes");
		// лампочка, серце, зірка, курсор
		var shapes = [
			"M47.5,65.1l35.7-48.7l61.1-7l-40.7,69.3L47.5,65.1z M83.8,155.3  l39.8-7.2l-20-69.5l-46.2,45.5L83.8,155.3z M193.2,50.3l-48.8-41l34,71.5L193.2,50.3z M123.7,148.2l54.7-67.3l-74.7-2.2L123.7,148.2  z M195.8,106.5l-17.5-25.7l-54.7,67.3L195.8,106.5z M157.7,155l-34-6.8l-1.8,65.3l22.5-12.5L157.7,155z M121.8,213.5l1.8-65.3  l-29.5,52.2L121.8,213.5z M57.5,124.2l46.2-45.5L47.5,65.1L57.5,124.2z M123.7,148.2l-39.8,7.2l10.3,45L123.7,148.2z M195.8,106.5  l-2.7-56.2l-14.8,30.5L195.8,106.5z M195.8,106.5l-72.2,41.7l34,6.8L195.8,106.5z M81.5,14v4.2h4V18h-4v-0.5h4V17h-4v-0.5h4V16h-4  v-0.5h4V15h-4v-0.5h4V14H82H81.5z M101.6,75.9v4.2h4v-0.2h-4v-0.5h4v-0.5h-4v-0.5h4v-0.5h-4v-0.5h4v-0.5h-4v-0.5h4v-0.5h-3.5H101.6z   M55.5,122v4.2h4V126h-4v-0.5h4V125h-4v-0.5h4V124h-4v-0.5h4V123h-4v-0.5h4V122H56H55.5z M193.5,104v4.2h4V108h-4v-0.5h4V107h-4  v-0.5h4V106h-4v-0.5h4V105h-4v-0.5h4V104H194H193.5z M92.5,196.9v4.2h4v-0.2h-4v-0.5h4v-0.5h-4v-0.5h4v-0.5h-4v-0.5h4v-0.5h-4v-0.5  h4v-0.5H93H92.5z",
			"M53.3,13.8l62.1,22.8l17.4,49.1L61.4,57.1L53.3,13.8z M30.4,95.3  l31-38.1l-60.9-4L30.4,95.3z M74.5,155.2l44.1,52.8l14.2-122.4L74.5,155.2z M132.8,85.7l25-23l28-58l-70.4,31.8L132.8,85.7z   M242.8,72.4l-85-9.7l21.3,80L242.8,72.4z M61.4,57.1l-8.1-43.4L0.5,53.2L61.4,57.1z M74.5,155.2l58.3-69.5L61.4,57.1l-31,38.1  L74.5,155.2z M118.6,208l60.5-65.3l-21.3-80l-25,23L118.6,208z M185.8,4.7l-28,58l85,9.7L185.8,4.7z M59.4,55v4.2h4V59h-4v-0.5h4V58  h-4v-0.5h4V57h-4v-0.5h4V56h-4v-0.5h4V55h-3.5H59.4z M155.8,60.4v4.2h4v-0.2h-4V64h4v-0.5h-4V63h4v-0.5h-4V62h4v-0.5h-4V61h4v-0.5  h-3.5L155.8,60.4L155.8,60.4z M240.5,70.7v4.2h4v-0.2h-4v-0.5h4v-0.5h-4v-0.5h4v-0.5h-4v-0.5h4v-0.5h-4v-0.5h4v-0.5H241H240.5z   M176.9,140.6v4.2h4v-0.2h-4v-0.5h4v-0.5h-4v-0.5h4v-0.5h-4v-0.5h4v-0.5h-4v-0.5h4v-0.5h-3.5H176.9z M116.8,205.1h3.5v0.5h-4v0.5h4  v0.5h-4v0.5h4v0.5h-4v0.5h4v0.5h-4v0.5h4v0.2h-4v-4.2H116.8z",
			"M12.5,82.2L85.6,65l4.1,37.1L12.5,82.2z M124.1,2.4l-34.5,99.6  L163.5,65L124.1,2.4z M147.5,143l-57.9-40.9l-30.4,34.6L147.5,143z M163.5,65l-16,78l86.6-60.5L163.5,65z M124.1,183.4l23.4-40.4  l-91.4,67.6L124.1,183.4z M187.1,139.5l-39.6,3.5l43,69.2L187.1,139.5z M89.6,102.1l34.5-99.6L85.6,65L89.6,102.1z M59.2,136.7  l30.4-34.6L12.5,82.2L59.2,136.7z M147.5,143l-88.3-6.3l-3.1,73.8L147.5,143z M124.1,183.4l66.5,28.8l-43-69.2L124.1,183.4z   M147.5,143l39.6-3.5l47-57L147.5,143z M10.9,80.4v4.2h4v-0.2h-4v-0.5h4v-0.5h-4v-0.5h4v-0.5h-4v-0.5h4v-0.5h-4v-0.5h4v-0.5h-3.5  H10.9z M122.1,0.5v4.2h4V4.5h-4V4h4V3.5h-4V3h4V2.5h-4V2h4V1.5h-4V1h4V0.5h-3.5H122.1z M145.1,141.4v4.2h4v-0.2h-4v-0.5h4v-0.5h-4  v-0.5h4v-0.5h-4v-0.5h4v-0.5h-4v-0.5h4v-0.5h-3.5H145.1z M188.9,209.3v4.2h4v-0.2h-4v-0.5h4v-0.5h-4v-0.5h4v-0.5h-4v-0.5h4v-0.5h-4  v-0.5h4v-0.5h-3.5H188.9z M87.5,100.2v4.2h4v-0.2h-4v-0.5h4v-0.5h-4v-0.5h4v-0.5h-4v-0.5h4v-0.5h-4v-0.5h4v-0.5H88H87.5z",
			"M50.5,117.6L194.7,4.1l0,192.9l-48-25.9l-27.4,37.5l-44.7-23.4  l16-43.2L50.5,117.6z M148.2,78.6l46.4-74.5L50.5,117.6L148.2,78.6z M194.6,197L148.2,78.6l-1.6,92.5L194.6,197z M101.7,169.1  l46.5-90.5L90.6,142l-16,43.2L101.7,169.1z M146.7,171.1l-45-2l17.6,39.5L146.7,171.1z M90.6,142l-40.2-24.4 M148.2,78.6L194.6,197  l0-192.9L148.2,78.6z M119.3,208.5l-17.6-39.5l-27.1,16.1L119.3,208.5z M146.3,76.4v4.2h4v-0.2h-4V80h4v-0.5h-4V79h4v-0.5h-4V78h4  v-0.5h-4V77h4v-0.5h-3.5H146.3z M49.1,115.5v4.2h4v-0.2h-4V119h4v-0.5h-4V118h4v-0.5h-4V117h4v-0.5h-4V116h4v-0.5h-3.5H49.1z   M72.2,183.1v4.2h4v-0.2h-4v-0.5h4v-0.5h-4v-0.5h4v-0.5h-4v-0.5h4v-0.5h-4v-0.5h4v-0.5h-3.5H72.2z M117.3,205.3v4.2h4v-0.2h-4v-0.5  h4v-0.5h-4v-0.5h4v-0.5h-4v-0.5h4v-0.5h-4v-0.5h4v-0.5h-3.5H117.3z"
		];
		var shapesShadows = [
			"M178.3,70.5L144.3-1l48.8,41L178.3,70.5z M83.8,145.8l39.8-7.2  l-20-69.5l-46.2,45.5L83.8,145.8z M157.7,146.5L195.8,98l-72.2,41.7L157.7,146.5z",
			"M132.9,79.6L118.7,202l-44.1-52.8L132.9,79.6z M61.5,51.4L132.9,80  l-17.4-49.1L53.4,8.1L61.5,51.4z M242.9,65.7L185.9-2l-28,58L242.9,65.7z",
			"M12.9,72.2L90,92.1l-30.4,34.6L12.9,72.2z M164,54.9l-16,78l86.6-60.5  L164,54.9z",
			"M74.9,179.1L102,163l17.6,39.5L74.9,179.1z M195,191.9L148.6,73.5  L147,166L195,191.9z M149.6,72.5L196-2L51.8,111.5L149.6,72.5z"
		];
		var shapeStyles = {
			fill: "none",
			stroke: "#000",
			strokeWidth: 1
		}
		var shadowStyles = {
			fill: "rgba(222,220,218,0.2)",
			strokeWidth: 1
		}
		function makeIterator(array){
			var nextIndex = 0;

			return {
				next: function(){
					if (nextIndex < array.length) {
						return array[nextIndex++];
					} else {
						nextIndex = 0;
					return array[nextIndex++];
					}
				}
			};
		}

		var currentEl = makeIterator(shapes);
		var currentShadow = makeIterator(shapesShadows);

		var elementShadow = s.select('#shadow');
		var element = s.select('#shape');

		var shapesLoop = setInterval(function(){
			element.animate({ d: currentEl.next() }, 500, mina.ease);
			elementShadow.animate({ d: currentShadow.next() }, 500, mina.ease);
		}, 3000);

	}
});
