/* globals google */
import Ember from 'ember';

export default Ember.Component.extend({
    didInsertElement() {
        this._resizeHandler = function() {
            let mapContainerHeight = parseInt(Ember.$('.connectedParticles').css('height')) - parseInt(Ember.$('.bottomMenu').css('height')) - parseInt(Ember.$('.connectedParticles').css('paddingTop'));
            this.$().css({
                'height': mapContainerHeight-1
            });
        }.bind(this);

        Ember.$(window).on('resize', this._resizeHandler);
        this._resizeHandler();


        CustomMarker.prototype = new google.maps.OverlayView();
        CustomMarker.prototype.draw = function() {
            var div = this.div_;
            if (!div) {
                div = this.div_ = Ember.$('<div class="rainbowBubble"><div class="inn mapBubbleBackground">Vivaki<div class="triangle-wrap"><div class="triangle mapBubbleBackground"></div></div></div></div>');
                Ember.$(this.getPanes().overlayImage).append(div);
            }

            var point = this.getProjection().fromLatLngToDivPixel(this.latlng_);
            if (point) {
                div.css({'left': point.x - 16 + 'px'});
                div.css({'top': point.y - 57 + 'px'});
            }
        };

        var latitude = this.get('latitude');
        var longtitude = this.get('longitude');

        function CustomMarker(latlng, map) {
            this.latlng_ = latlng;
            this.setMap(map);
        }

        var options = {
            styles: [{featureType:"all",elementType:"labels.text.fill",stylers:[{saturation:36},{color:"#000000"},{lightness:40}]},{featureType:"all",elementType:"labels.text.stroke",stylers:[{visibility:"on"},{color:"#000000"},{lightness:16}]},{featureType:"all",elementType:"labels.icon",stylers:[{visibility:"off"}]},{featureType:"administrative",elementType:"geometry.fill",stylers:[{color:"#000000"},{lightness:20}]},{featureType:"administrative",elementType:"geometry.stroke",stylers:[{color:"#000000"},{lightness:17},{weight:1.2}]},{featureType:"landscape",elementType:"all",stylers:[{visibility:"on"}]},{featureType:"landscape",elementType:"geometry",stylers:[{color:"#000000"},{lightness:20}]},{featureType:"landscape",elementType:"labels.icon",stylers:[{saturation:"-100"},{lightness:"-54"}]},{featureType:"poi",elementType:"all",stylers:[{visibility:"on"},{lightness:"0"}]},{featureType:"poi",elementType:"geometry",stylers:[{color:"#000000"},{lightness:21}]},{featureType:"poi",elementType:"labels.icon",stylers:[{saturation:"-89"},{lightness:"-55"}]},{featureType:"road",elementType:"labels.icon",stylers:[{visibility:"off"}]},{featureType:"road.highway",elementType:"geometry.fill",stylers:[{color:"#000000"},{lightness:17}]},{featureType:"road.highway",elementType:"geometry.stroke",stylers:[{color:"#000000"},{lightness:29},{weight:0.2}]},{featureType:"road.arterial",elementType:"geometry",stylers:[{color:"#000000"},{lightness:18}]},{featureType:"road.local",elementType:"geometry",stylers:[{color:"#000000"},{lightness:16}]},{featureType:"transit",elementType:"geometry",stylers:[{color:"#000000"},{lightness:19}]},{featureType:"transit.station",elementType:"labels.icon",stylers:[{visibility:"on"},{saturation:"-100"},{lightness:"-51"}]},{featureType:"water",elementType:"geometry",stylers:[{color:"#000000"},{lightness:17}]}],
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            zoom: 16,
            center: new google.maps.LatLng(latitude, longtitude),
            backgroundColor: '#E5E3DF'
        };
        var map = new window.google.maps.Map(this.$('.map-canvas')[0], options);
        new CustomMarker(new google.maps.LatLng(latitude, longtitude), map);
    },
    willDestroyElement() {
        Ember.$(window).off('resize', this._resizeHandler);
    },
});
