import Ember from 'ember';

export default Ember.Component.extend({
	didInsertElement: function() {
		function whichTransitionEvent() {
			var el = document.createElement('fake'),
				transEndEventNames = {
					'WebkitTransition' : 'webkitTransitionEnd',
					'MozTransition'    : 'transitionend',
					'transition'       : 'transitionend'
				};

			for(var t in transEndEventNames){
				if( el.style[t] !== undefined ){
					return transEndEventNames[t];
				}
			}
		}

		var transEndEventName = whichTransitionEvent();

		/*
		** Slider
		*/

		// all slides are hided initialy, we need to show first slide on start
		var firstLogotype = $('.slider .item:first-child').addClass('show').find('svg').attr("class", "");

		// slider navigation uses event delegation
		$('.slider-nav').on('click', function(e){
			var currentEl = $('.show'),
				currentElindex = currentEl.index(),
				els = $('.companies-grid .item'),
				elsTotal = $('.companies-grid .item').length,
				nextEl,
				nextDot;
			// console.log($(e.target));
			if ($(e.target).hasClass("slider-arrow")) {
				if ($(e.target).hasClass("down")) {
					currentElindex++;
				}

				if ($(e.target).hasClass("up")) {
					currentElindex--;
				}

				nextEl = $('.companies-grid .item').get(currentElindex);
				if(nextEl) {
					$(currentEl)
						.find('svg')
						.attr("class", "explode")
						.one(transEndEventName, function(e) {
							// console.log('+', e);
							nextDot = $('.dot-wrap').get(currentElindex);

							$('.item').removeClass('show');
							$('.dot-wrap').removeClass('current');
							$(nextEl).addClass('show').find('svg').attr("class", "");
							$(nextDot).addClass('current');

						});
				} else {
					return false;
				}

			}

			// list navigation
			if ($(e.target).hasClass("list-arrow")) {
				var viewportHeight = $(document).height(),
					elHeiht = $('.companies-grid .item:first-child').outerHeight(),
					showedElements = Math.floor(viewportHeight/showedElements),
					scrollStep;

				if ($(e.target).hasClass("down")) {
					scrollStep  = $('.companies-grid').scrollTop() + elHeiht
				}
				if ($(e.target).hasClass("up")) {
					var scrollStep  = $('.companies-grid').scrollTop() - elHeiht;
				}
				$('.companies-grid').animate({
					scrollTop: scrollStep
				}, 1600);
			}

			if ($(e.target).hasClass("change-view") || e.target.tagName.toLowerCase() == 'span') {

				if (e.target.tagName.toLowerCase() == 'span' || $(e.target).hasClass("btn-grid")) {
					els.each(function(){
						$(this).find('svg').attr("class", "");
					});
				} else {
					els.each(function(){
						$(this).find('svg').attr("class", "exploded");
					});
					$(currentEl).find('svg').attr("class", "");
				}
				$('.change-view').toggleClass('btn-close');
				$('.change-view').toggleClass('btn-grid');
				$(".arrows").toggleClass('hidden');
				$('.companies-grid').toggleClass('slider');
				$('.companies-grid').toggleClass('list');
			}

			e.stopPropagation();
		});
		var dots = $('.dot-wrap');
		$(dots[0]).addClass('current');
		$('.dot-wrap').on('click', function() {
			var currentEl = $('.show'),
				newCurrentElindex = $(this).index(),
				newCurrentEl  = $('.companies-grid .item').get(newCurrentElindex),
				newCurrentDot = $(this);

			$(currentEl)
				.find('svg')
				.attr("class", "explode")
				.one(transEndEventName, function(e) {
					$('.item').removeClass('show');
					$('.dot-wrap').removeClass('current');
					$(newCurrentEl).addClass('show').find('svg').attr("class", "");
					$(newCurrentDot).addClass('current');
			});
		});
		var textillate_settings = {
			selector: '.text',
			in: {
				effect: 'fadeInUp',
				sync: true,
				delay: 30
			},
			out: {
				effect: 'fadeOutRight',
				sync: false,
				delay: 30
			},
			outEffects: [ 'hinge' ],
			autoStart: false,
			type: 'char'
		};
		// animation for text
		$('.text').textillate(textillate_settings);

		$('.dot-wrap .dot').hover(
			function () {
				var dot_wrap = $(this).parent();
				if (!$(dot_wrap).hasClass('current')) {
					var current = $(dot_wrap).find('.text');
					current.textillate('in');
				}
			},
			function(){
				var dot_wrap = $(this).parent();
				if (!$(dot_wrap).hasClass('.current')) {
					var current = $(dot_wrap).find('.text');
					current.textillate('out');
				}
			});
	}
});
