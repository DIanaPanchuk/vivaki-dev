import Ember from 'ember';
import Validation from 'vivaki/mixins/validation';

export default Ember.Component.extend(Validation, {
	didInsertElement: function() {
		this.validate();
		if ($('body').hasClass('feedback-view')) {
			Ember.$('.hamburger').addClass('opened');
		}
	},
	actions: {
		submit: function() {
			this.sendAction('action', this.get('contact'));
		}
	}
});
