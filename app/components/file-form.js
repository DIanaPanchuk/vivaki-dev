import Ember from 'ember';
import Validation from 'vivaki/mixins/validation';

export default Ember.Component.extend(Validation, {
	didInsertElement: function() {
		this.validate();
	},
	actions: {
		submit: function() {
			console.log(this);
			this.sendAction('action', this.get('job'));
		}
	}
});
