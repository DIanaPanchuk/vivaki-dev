import Ember from 'ember';

export default Ember.Component.extend({
	classNames: ['country-wrap'],
	didInsertElement: function(){
		var carouselBreakpoints = {
			mobile: 3,
			tablets: 4,
			screens: 9
		};
		var carousel = $('.owl-carousel').owlCarousel({
			items: carouselBreakpoints.mobile,
			dots: false,
			margin: 14,
			nav: false,
			responsive: {
				768: {
					items: carouselBreakpoints.tablets,
					margin: 12
				},
				960: {
					items: carouselBreakpoints.screens,
					margin: 10
				},
				1280: {
					items: carouselBreakpoints.screens,
					margin: 15
				}
			},
			onInitialized: function() {
				$('.owl-stage').on('click', function(e){
					$('.worldMap, .hero, .spacer').addClass('hidden');
					$('.content-inner').removeClass('show-desc');
					var country = $(e.target).closest('.owl-item');
					$('.owl-item.current').removeClass('current');

					var currentCountryIndex = country.index();
					var currentImage = $('.info-box').get(currentCountryIndex);
					var currentDesc = $('.description').get(currentCountryIndex);

					$('.info-box').removeClass('current');
					$('.description.current').removeClass('current');

					$(country).addClass('current');
					$(currentImage).addClass('current');
					$(currentDesc).addClass('current');
				});
			},
			onRefreshed: function(){
				var screenWidth = $(document).width();
				var carouselEls = $('.owl-item').length;

				// hide carousel arrow on differense screens
				if (screenWidth < 768) {
					if (carouselEls <= carouselBreakpoints.mobile) {
							$('.carousel-nav').addClass('disabled');
					} else {
						$('.carousel-nav').removeClass('disabled');
					}
				} else if (screenWidth >= 768 && screenWidth < 960) {
					if (carouselEls <= carouselBreakpoints.tablets) {
						$('.carousel-nav').addClass('disabled');
					} else {
						$('.carousel-nav').removeClass('disabled');
					}
				} else {
					if (carouselEls <= carouselBreakpoints.screens) {
						$('.carousel-nav').addClass('disabled');
					} else {
						$('.carousel-nav').removeClass('disabled');
					}
				}
			}
		});
		//custom carousel navigation
		$('.carousel-next').click(function() {
			carousel.trigger('next.owl.carousel');
		});
		$('.carousel-prev').click(function() {
			carousel.trigger('prev.owl.carousel');
		});


		$('.image-wrap .info-box').on('click', function(){
			if ($('.description').hasClass('current')) {
				$('.content-inner').toggleClass('show-desc');

				$('body').addClass('open');
				$('.logoContainer').addClass('invisible');
			}
		});
		$('.btn-close').on('click', function(){
			$('.content-inner').toggleClass('show-desc');
			$('body').removeClass('open');
			$('.logoContainer').removeClass('invisible');
		});


		// scroll
		$('.arrows').on('click', function(e){
			var currentScrollPosition = $('.description.current').scrollTop();
			if ($(e.target).hasClass('up')) {
				$('.description.current').animate({
					scrollTop: currentScrollPosition - 100
				}, 500);
			} else {
				$('.description.current').animate({
					scrollTop: currentScrollPosition + 100
				}, 500);
			}
		});
	}
});
