/* globals d3 */
import Ember from 'ember';

export default Ember.Component.extend({
    tagName: 'canvas',
    attributeBindings: ['width', 'height'],
    classNames: ['scene'],
    didInsertElement: function() {
        //auto resize container
        this._resizeHandler = function() {
            Ember.$('.sphere').css({
                'height': Ember.$('body').css('height')
            });
            let el = Ember.$(this.get('element')),
                sphere = Ember.$('.sphere');
            if (parseInt(sphere.css('width')) > parseInt(sphere.css('height'))) {
                el.removeClass('matchHorizontal');
                el.addClass('matchVertical');
            } else {
                el.removeClass('matchVertical');
                el.addClass('matchHorizontal');
            }
        }.bind(this);
        Ember.$(window).on('resize', this._resizeHandler);
        this._resizeHandler();

        //d3 geodesic plugin
        (function() {
            var φ = 1.618033988749895,
                ρ = 180 / Math.PI;

            var vertices = [
                [1, φ, 0],
                [-1, φ, 0],
                [1, -φ, 0],
                [-1, -φ, 0],
                [0, 1, φ],
                [0, -1, φ],
                [0, 1, -φ],
                [0, -1, -φ],
                [φ, 0, 1],
                [-φ, 0, 1],
                [φ, 0, -1],
                [-φ, 0, -1]
            ];

            var faces = [
                [0, 1, 4],
                [1, 9, 4],
                [4, 9, 5],
                [5, 9, 3],
                [2, 3, 7],
                [3, 2, 5],
                [7, 10, 2],
                [0, 8, 10],
                [0, 4, 8],
                [8, 2, 10],
                [8, 4, 5],
                [8, 5, 2],
                [1, 0, 6],
                [11, 1, 6],
                [3, 9, 11],
                [6, 10, 7],
                [3, 11, 7],
                [11, 6, 7],
                [6, 0, 10],
                [9, 1, 11]
            ].map(function(face) {
                return face.map(function(i) {
                    return vertices[i];
                });
            });

            var faces2 = faces;

            d3.geodesic = {
                multipolygon: function(n) {
                    return {
                        type: "MultiPolygon",
                        coordinates: subdivideFaces(~~n).map(function(face) {
                            face = face.map(project);
                            face.push(face[0]);
                            face = [face];
                            return face;
                        })
                    };
                },
                polygons: function(n) {
                    return d3.geodesic.multipolygon(~~n).coordinates.map(function(face) {
                        return {
                            type: "Polygon",
                            coordinates: face
                        };
                    });
                },
                multilinestring: function(n) {
                    return {
                        type: "MultiLineString",
                        coordinates: subdivideEdges(~~n).map(function(edge) {
                            return edge.map(project);
                        })
                    };
                }
            };

            function subdivideFaces(n) {
                return d3.merge(faces.map(function(face) {
                    var i01 = interpolate(face[0], face[1]),
                        i02 = interpolate(face[0], face[2]),
                        faces = [];

                    faces.push([
                        face[0],
                        i01(1 / n),
                        i02(1 / n)
                    ]);

                    for (var i = 1; i < n; ++i) {
                        var i1 = interpolate(i01(i / n), i02(i / n)),
                            i2 = interpolate(i01((i + 1) / n), i02((i + 1) / n));
                        for (var j = 0; j <= i; ++j) {
                            faces.push([
                                i1(j / i),
                                i2(j / (i + 1)),
                                i2((j + 1) / (i + 1))
                            ]);
                        }
                        for (var k = 0; k < i; ++k) {
                            faces.push([
                                i1(k / i),
                                i2((k + 1) / (i + 1)),
                                i1((k + 1) / i)
                            ]);
                        }
                    }

                    return faces;
                }));
            }

            function subdivideEdges(n) {
                var edges = {};

                subdivideFaces(n).forEach(function(face) {
                    add(face[0], face[1]);
                    add(face[1], face[2]);
                    add(face[2], face[0]);
                });

                function add(p0, p1) {
                    var t;
                    if (p0[0] < p1[0] || (p0[0] === p1[0] && (p0[1] < p1[1] || (p0[1] === p1[1] && p0[2] < p1[2])))) {
                        t = p0;
                        p0 = p1;
                        p1 = t;
                    }
                    edges[p0.map(round) + " " + p1.map(round)] = [p0, p1];
                }

                function round(d) {
                    return d3.round(d, 4);
                }

                return d3.values(edges);
            }

            function interpolate(p0, p1) {
                var x0 = p0[0],
                    y0 = p0[1],
                    z0 = p0[2],
                    x1 = p1[0] - x0,
                    y1 = p1[1] - y0,
                    z1 = p1[2] - z0;
                return function(t) {
                    return [
                        x0 + t * x1,
                        y0 + t * y1,
                        z0 + t * z1
                    ];
                };
            }

            function project(p) {
                var x = p[0],
                    y = p[1],
                    z = p[2];
                return [
                    Math.atan2(y, x) * ρ,
                    Math.acos(z / Math.sqrt(x * x + y * y + z * z)) * ρ - 90
                ];
            }
        })();

        //icosahedron
        var canvas = this.get('element');
        var width = canvas.offsetWidth,
            height = canvas.offsetWidth,
            radius = (width / 1.1) - (width/100*10),
            radius2 = (width / 1.4  ) - (width/100*10);
        var velocity = [0.0005, 0.0007, 0.0009],
            t0 = Date.now();
        var projection = d3.geo.orthographic()
            .scale(radius)
            .translate([width / 1, height / 1]);
        var projection2 = d3.geo.orthographic()
            .scale(radius2)
            .translate([width / 1, height / 0.925]);
        var context = canvas.getContext("2d");

        canvas.width = width * 2;
        canvas.height = height * 2;
        // retina detect
        // if (window.devicePixelRatio > 1) {
        //     canvas.width = width * window.devicePixelRatio;
        //     canvas.height = height * window.devicePixelRatio;
        //     canvas.style.width = width;
        //     canvas.style.height = height;
        //     context.scale(window.devicePixelRatio, window.devicePixelRatio);
        // }

        context.lineWidth = 0.5;
        var faces, faces2;

        geodesic();

        d3.timer(function() {
          var time = Date.now() - t0;
          projection.rotate([time * velocity[0], time * velocity[1], time * velocity[2]]);
          projection2.rotate([time * velocity[0], time * velocity[1], time * velocity[2]]);
          redraw();
        });

        function redraw() {
            context.clearRect(0, 0, width * 2, height * 2);
            faces.forEach(function(d) {
                context.lineWidth = 1;
                d.polygon[0] = projection(d[0]);
                d.polygon[1] = projection(d[1]);
                d.polygon[2] = projection(d[2]);

                context.beginPath();
                if (d.visible = d.polygon.area() > 0) {
                    context.strokeStyle = "#999999";
                } else {
                    context.strokeStyle = "#bfbfbe";
                }
                context.fillStyle = d.fill;
                drawTriangle(d.polygon);
                context.fill();
                context.stroke();

                if (d.fill === "rgba(0,0,0,0.1)" && d.visible) {
                    d.fill = 'rgba(120,120,120,0.1)';
                }
                if (d.fill === "rgba(120,120,120,0.1)" && !d.visible) {
                    d.fill = 'rgba(0,0,0,0.1)';
                }
            });
            faces2.forEach(function(d) {
                context.lineWidth = 1;

                d.polygon[0] = projection2(d[0]);
                d.polygon[1] = projection2(d[1]);
                d.polygon[2] = projection2(d[2]);
                d.polygon[0][1] = d.polygon[0][1] - 85;
                d.polygon[1][1] = d.polygon[1][1] - 85;
                d.polygon[2][1] = d.polygon[2][1] - 85;
                d.polygon[0][0] = d.polygon[0][0] - 100;
                d.polygon[1][0] = d.polygon[1][0] - 100;
                d.polygon[2][0] = d.polygon[2][0] - 100;

                if(d.visible = d.polygon.area() > 0){
                    context.fillStyle = '#000';
                    context.fillRect(d.polygon[0][0]-2.5, d.polygon[0][1]-2.5, 5, 5);
                }

                context.beginPath();
                if (d.visible = d.polygon.area() > 0) {
                    context.strokeStyle = "#000";
                } else {
                    context.strokeStyle = "#bfbfbe";
                }
                context.fillStyle = d.fill;
                drawTriangle(d.polygon);
                context.fill();
                context.stroke();

                if (d.fill === "rgba(0,0,0,0.1)" && d.visible) {
                    d.fill = 'rgba(120,120,120,0.1)';
                }
                if (d.fill === "rgba(120,120,120,0.1)" && !d.visible) {
                    d.fill = 'rgba(0,0,0,0.1)';
                }
            });
            context.beginPath();
            faces.forEach(function(d) {
                // if (d.visible) {
                drawTriangle(d.polygon);
                // }
            });
        }

        function drawTriangle(triangle) {
            context.moveTo(triangle[0][0], triangle[0][1]);
            context.lineTo(triangle[1][0], triangle[1][1]);
            context.lineTo(triangle[2][0], triangle[2][1]);
            context.closePath();
        }

        function geodesic() {
            // output.text(subdivision);
            faces = d3.geodesic.polygons(3).map(function(d) {
                d = d.coordinates[0];
                d.pop(); // use an open polygon
                // d.fill = d3.hsl(d[0][0], 1, 0.5) + "";
                d.fill = randexec(["rgba(0,0,0,0.1)", "rgba(0,0,0,0)"], [3, 97]);
                d.polygon = d3.geom.polygon(d.map(projection));
                return d;
            });
            faces2 = d3.geodesic.polygons(1).map(function(d) {
                d = d.coordinates[0];
                d.pop(); // use an open polygon
                // d.fill = d3.hsl(d[0][0], 1, 0.5) + "";
                d.fill = randexec(["rgba(0,0,0,0.1)", "rgba(0,0,0,0)"], [3, 97]);
                d.polygon = d3.geom.polygon(d.map(projection));
                return d;
            });
            redraw();
        }

        //probability
        function randexec(what, prob) {
            var ar = [];
            var i, sum = 0;
            for (i = 0; i < prob.length - 1; i++) {
                sum += (prob[i] / 100.0);
                ar[i] = sum;
            }
            var r = Math.random(); // returns [0,1]
            for (i = 0; i < ar.length && r >= ar[i]; i++) {}
            return what[i];
        }
    }
});
