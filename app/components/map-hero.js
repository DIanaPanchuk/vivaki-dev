import Ember from 'ember';

export default Ember.Component.extend({
	didInsertElement: function() {
		if (Ember.$(document).width() >= 960) {
			Ember.$(".mapOfPresence").niceScroll({
				cursorwidth  : 0
			});
		}
		//number animation
		$('.title').animateNumber({ number: $('.title').data('number')}, 4000);

		// scrollmagic
		var scrollMagicController = new ScrollMagic.Controller();

		// tweens
		var opacity = TweenMax.to('.pinned', 2, {
			opacity: 0
		});
		var scale = TweenMax.to('.worldMap', 2, {
			scale: 2,
			x:-220,
			y: 150,
			force3D:false
		});
		var letterSpacing = TweenMax.to('.title', 2, {
			letterSpacing: '-0.6em',
			autoRound: false
		});
		var padding = TweenMax.to('.subTitle', 2, {
			paddingTop: 30
		});
		var pulse_rays = TweenMax.to('.pulse_rays', 2, {
			opacity: 1
		});

		// timeline
		var timeline = new TimelineMax()
			.add([opacity, scale, padding, letterSpacing, pulse_rays]);

		//scene
		var mapTitle = new ScrollMagic.Scene({
					triggerElement: '.hero',
					triggerHook: 'onLeave',
					offset: 0,
					duration: 300
				})
				.setTween(timeline)
				.addTo(scrollMagicController);


		// $('.mapOfPresence').on('mousewheel', function(event) {
		// 	console.log('+');
		// 	if ($(".mapOfPresence").scrollTop() == $('.mapOfPresence > div:first-child').height()) {
		// 		$('.pulse_rays').css({'opacity': '1'});
		// 	} else {
		// 		$('.pulse_rays').css({'opacity': '0'});
		// 	}
		// });

		// Add debug indicators fixed on right side
		// mapTitle.addIndicators();

		// var $window = $('.mapOfPresence');
		// var scrollTime = 1.2;
		// var scrollDistance = 170;

		// $window.on("mousewheel DOMMouseScroll", function(event){
		//  event.preventDefault();
		//  var delta = event.originalEvent.wheelDelta/120 || -event.originalEvent.detail/3;
		//  var scrollTop = $window.scrollTop();
		//  var finalScroll = scrollTop - parseInt(delta*scrollDistance);
		//  TweenMax.to($window, scrollTime, {
		//      scrollTo : { y: finalScroll, autoKill:true },
		//      ease: Power1.easeOut,
		//      overwrite: 5
		//    });
		// });
	}
});
