/* globals d3 */
import Ember from 'ember';
import snap from 'snap-svg';
// import mina from 'mina';

export default Ember.Component.extend({
    classNames: ['worldMap'],
    didInsertElement: function(){
        let s = snap('#world_map'),
            pathSet = s.selectAll('path');
        //auto resize container
        this._resizeHandler=function(){
            this.$().css({'height': Ember.$('.connectedParticles').css('height')});
        }.bind(this);
        Ember.$(window).on('resize', this._resizeHandler);
        this._resizeHandler();

        //random fill map triangles
        for (let i=0; i < 80; i++) {
            pathSet[Math.floor(Math.random() * pathSet.length)].attr({'class': 'st0 gray-1'});
            pathSet[Math.floor(Math.random() * pathSet.length)].attr({'class': 'st0 gray-2'});
            pathSet[Math.floor(Math.random() * pathSet.length)].attr({'class': 'st0 gray-3'});
        }

        // pathSet.forEach(function(el) {
        //     el.hover(
        //         function() {
        //             this.attr({ fill: '#ebebeb' });
        //         },
        //         function() {
        //             this.attr({ fill: 'none' });
        //         }
        //     );
        // });


        this._getPointCartesian =(φ, λ, bBox)=> {
            let mapWidth = bBox.width;
            let mapHeight = bBox.height;
            // convert from degrees to radians
            let latRad = φ*Math.PI/180;
            // get y value
            let mercN = Math.log(Math.tan((Math.PI/4)+(latRad/2)));
            return {
                x: (λ+180)*(mapWidth/360),
                y: (mapHeight/2)-((mapWidth*mercN)/(2*Math.PI))
            };
        };

        // let coordinatesArray = [
        //         {lat: 51.165100562205744, lng: 71.48423651738278},
        //         {lat: 40.177387389653234, lng: 44.499497213793916},
        //         {lat: 40.40822477078235, lng: 49.86245612187497},
        //         {lat: 41.70055979202109, lng: 44.81973151249997}
        //     ],
        let coordinatesArray = this.get('coordinatesArray'),
            pointsGroup = s.g(),
            bBox = s.getBBox();

        // coordinatesArray.push({lat:0, lng: 0});
        console.log(coordinatesArray);
        coordinatesArray.forEach(coord => {
            let cartesian = this._getPointCartesian(coord.lat, coord.lng, bBox);
            pointsGroup.add(s.circle(cartesian.x, cartesian.y, 5).attr('class','pulse_rays'));
        });
        /* TODO: fill all circles */
        pointsGroup.attr({
			fill: "#242021",
			stroke: "rgba(0,0,0,0.3)"
        });

    },
    willDestroyElement: function(){
      Ember.$(window).off('resize', this._resizeHandler);
    }
});
