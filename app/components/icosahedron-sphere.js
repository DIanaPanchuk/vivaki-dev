/* globals d3, $ */
import Ember from 'ember';

export default Ember.Component.extend({
    tagName: 'canvas',
    attributeBindings: ['width', 'height', 'company'],
    classNames: ['scene'],
    didInsertElement: function() {
        // get companies list
        var companies = this.get('company');
        var company_length = Object.keys(companies).length;

        // auto resize container
        this._resizeHandler = function() {
            var sphereHeigth = $(window).height() - $('.bottomMenu').outerHeight() - $('.anim.t').outerHeight();
            Ember.$('.sphere').css({
                'height': sphereHeigth + 'px',
                'line-height': sphereHeigth + 'px'
            });

            let el = Ember.$(this.get('element')),
                sphere = Ember.$('.sphere');
            if (parseInt(sphere.css('width')) > parseInt(sphere.css('height'))) {
                el.removeClass('matchHorizontal');
                el.addClass('matchVertical');
            } else {
                el.removeClass('matchVertical');
                el.addClass('matchHorizontal');
            }

            var totalWidth = Ember.$('.sphere').width();
            var sphereWidth = $(window).height() - $('.bottomMenu').outerHeight() - $('.anim.t').outerHeight();
            var padding = (((totalWidth-sphereWidth))+(sphereWidth/2));
            if ($('body').hasClass('about-view')) {
                $('.sphere canvas').css({'padding-left': padding});
            }
        }.bind(this);
        Ember.$(window).on('resize', this._resizeHandler);
        this._resizeHandler();

        //d3 geodesic plugin
        (function() {
            var φ = 1.618033988749895,
                ρ = 180 / Math.PI;

            var vertices = [
                [1, φ, 0],
                [-1, φ, 0],
                [1, -φ, 0],
                [-1, -φ, 0],
                [0, 1, φ],
                [0, -1, φ],
                [0, 1, -φ],
                [0, -1, -φ],
                [φ, 0, 1],
                [-φ, 0, 1],
                [φ, 0, -1],
                [-φ, 0, -1]
            ];

            var faces = [
                [0, 1, 4],
                [1, 9, 4],
                [4, 9, 5],
                [5, 9, 3],
                [2, 3, 7],
                [3, 2, 5],
                [7, 10, 2],
                [0, 8, 10],
                [0, 4, 8],
                [8, 2, 10],
                [8, 4, 5],
                [8, 5, 2],
                [1, 0, 6],
                [11, 1, 6],
                [3, 9, 11],
                [6, 10, 7],
                [3, 11, 7],
                [11, 6, 7],
                [6, 0, 10],
                [9, 1, 11]
            ].map(function(face) {
                return face.map(function(i) {
                    return vertices[i];
                });
            });

            var faces2 = faces;

            d3.geodesic = {
                multipolygon: function(n) {
                    return {
                        type: "MultiPolygon",
                        coordinates: subdivideFaces(~~n).map(function(face) {
                            face = face.map(project);
                            face.push(face[0]);
                            face = [face];
                            return face;
                        })
                    };
                },
                polygons: function(n) {
                    return d3.geodesic.multipolygon(~~n).coordinates.map(function(face) {
                        return {
                            type: "Polygon",
                            coordinates: face
                        };
                    });
                },
                multilinestring: function(n) {
                    return {
                        type: "MultiLineString",
                        coordinates: subdivideEdges(~~n).map(function(edge) {
                            return edge.map(project);
                        })
                    };
                }
            };

            function subdivideFaces(n) {
                return d3.merge(faces.map(function(face) {
                    var i01 = interpolate(face[0], face[1]),
                        i02 = interpolate(face[0], face[2]),
                        faces = [];

                    faces.push([
                        face[0],
                        i01(1 / n),
                        i02(1 / n)
                    ]);

                    for (var i = 1; i < n; ++i) {
                        var i1 = interpolate(i01(i / n), i02(i / n)),
                            i2 = interpolate(i01((i + 1) / n), i02((i + 1) / n));
                        for (var j = 0; j <= i; ++j) {
                            faces.push([
                                i1(j / i),
                                i2(j / (i + 1)),
                                i2((j + 1) / (i + 1))
                            ]);
                        }
                        for (var k = 0; k < i; ++k) {
                            faces.push([
                                i1(k / i),
                                i2((k + 1) / (i + 1)),
                                i1((k + 1) / i)
                            ]);
                        }
                    }

                    return faces;
                }));
            }

            function subdivideEdges(n) {
                var edges = {};

                subdivideFaces(n).forEach(function(face) {
                    add(face[0], face[1]);
                    add(face[1], face[2]);
                    add(face[2], face[0]);
                });

                function add(p0, p1) {
                    var t;
                    if (p0[0] < p1[0] || (p0[0] === p1[0] && (p0[1] < p1[1] || (p0[1] === p1[1] && p0[2] < p1[2])))) {
                        t = p0;
                        p0 = p1;
                        p1 = t;
                    }
                    edges[p0.map(round) + " " + p1.map(round)] = [p0, p1];
                }

                function round(d) {
                    return d3.round(d, 4);
                }

                return d3.values(edges);
            }

            function interpolate(p0, p1) {
                var x0 = p0[0],
                    y0 = p0[1],
                    z0 = p0[2],
                    x1 = p1[0] - x0,
                    y1 = p1[1] - y0,
                    z1 = p1[2] - z0;
                return function(t) {
                    return [
                        x0 + t * x1,
                        y0 + t * y1,
                        z0 + t * z1
                    ];
                };
            }

            function project(p) {
                var x = p[0],
                    y = p[1],
                    z = p[2];
                return [
                    Math.atan2(y, x) * ρ,
                    Math.acos(z / Math.sqrt(x * x + y * y + z * z)) * ρ - 90
                ];
            }
        })();

        //icosahedron
        var factor = 1.4;
        var amendment = 0;
        if (navigator.userAgent.indexOf ('Windows') != -1) amendment = 1.5;

        var canvas = this.get('element');
        var context = canvas.getContext("2d");
        var width = canvas.offsetWidth,
            height = canvas.offsetWidth,
            radius = (width / factor) - (width/100*10),
            radius2 = (width / 2) - (width/100*10);
        var velocity = [0.04, 0.07], division = [velocity[0] / 50, velocity[1] / 50];

        var window_width = Ember.$(window).width();
        var icon_width, plus_width;
        if(window_width >= 1680){
            icon_width = 38;
            plus_width = 15;
        } else if(window_width >= 1280) {
            icon_width = 36;
            plus_width = 15;
        } else if(window_width >= 960) {
            icon_width = 33;
            plus_width = 11;
        } else if(window_width >= 768) {
            icon_width = 38;
            plus_width = 15;
        } else if(window_width >= 320) {
            icon_width = 28;
            plus_width = 11;
        } else {
            icon_width = 28;
            plus_width = 11;
        }

        var projection = d3.geo.orthographic()
            .scale(radius)
            .translate([width / 1.4, height / 1.4]);

        var projection2 = d3.geo.orthographic()
            .scale(radius2)
            .translate([width / 1.4, height / 1.4]);

        canvas.width = width * factor;
        canvas.height = height * factor;

        // retina detect
        // if (window.devicePixelRatio > 1) {
        //     canvas.width = width * window.devicePixelRatio;
        //     canvas.height = height * window.devicePixelRatio;
        //     canvas.style.width = width;
        //     canvas.style.height = height;
        //     context.scale(window.devicePixelRatio, window.devicePixelRatio);
        // }

        // sphere line width
        context.lineWidth = 0.5;
        var faces;var faces2;

        // subdivide
        geodesic();

        var pointsArray = [];
        var icon_hover = $('.sphereHover');
        var icon_count = 0;
        var company_id = 0;
        var isPlusShow = true;
        var hoverStatus = false;
        var hoverShow = false;
        var hoverX, hoverY;

        // redraw scene
        function redraw() {
            context.clearRect(0, 0, width * factor, height * factor);

            (function drawSlogan(){
                //textPoints
                // V[width * 35 / 870, width * 516 / 870],
                // I[width * 364 / 870, width * 212 / 870],
                // V[width * 762 / 870, width * 339 / 870],
                // A[width * 1104 / 870, width * 442 / 870],
                // K[width * 1085 / 870, width * 861 / 870],
                // I[width * 844 / 870, width * 1122 / 870]

                var pointsSlogan = [
                    [-90,11],
                    [-48,49.98],
                    [161.82,31.88],
                    [90,20.9],
                    [78,-27,75],
                    [90,-65.44],
                ];
                var pointsSloganProjection = [];
                var fontSize = width * 120 / 870;

                pointsSloganProjection[0] = projection(pointsSlogan[0]);
                pointsSloganProjection[1] = projection(pointsSlogan[1]);
                pointsSloganProjection[2] = projection(pointsSlogan[2]);
                pointsSloganProjection[3] = projection(pointsSlogan[3]);
                pointsSloganProjection[4] = projection(pointsSlogan[4]);
                pointsSloganProjection[5] = projection(pointsSlogan[5]);

                context.font = "bold "+fontSize+"px Arial";
                context.fillStyle = "rgba(121,121,121,.9)";
                context.fillText("V",pointsSloganProjection[0][0], pointsSloganProjection[0][1]);
                context.fillStyle = "rgba(121,121,121,.7)";
                context.fillText("I",pointsSloganProjection[1][0], pointsSloganProjection[1][1]);
                context.fillStyle = "rgba(121,121,121,.9)";
                context.fillText("V",pointsSloganProjection[2][0], pointsSloganProjection[2][1]);
                context.fillStyle = "rgba(121,121,121,.55)";
                context.fillText("A",pointsSloganProjection[3][0], pointsSloganProjection[3][1]);
                context.fillStyle = "rgba(121,121,121,.9)";
                context.fillText("K",pointsSloganProjection[4][0], pointsSloganProjection[4][1]);
                context.fillStyle = "rgba(121,121,121,.7)";
                context.fillText("I",pointsSloganProjection[5][0], pointsSloganProjection[5][1]);
            })();

            pointsArray = [];
            var rectColors = ['#00b0f3','#fb9a00','#b69960','#cb1818'];
            icon_count = 0;
            company_id = 0;

            //largeSphere
            var text_count = 0;
            faces.forEach(function(d) {
                context.lineWidth = 1;
                d.polygon[0] = projection(d[0]);
                d.polygon[1] = projection(d[1]);
                d.polygon[2] = projection(d[2]);

                context.beginPath(d.polygon[0]);
                if (d.visible = d.polygon.area() > 0) {
                    context.strokeStyle = "#ada8a3";
                } else {
                    context.strokeStyle = "#bfbfbe";
                }
                context.fillStyle = d.fill;
                drawTriangle(d.polygon);

                if (d.fill === "rgba(0,0,0,0.1)" && d.visible) {
                    d.fill = 'rgba(0,0,0,0.1)';
                }
                if (d.fill === "rgba(0,0,0,0.1)" && !d.visible) {
                    d.fill = 'rgba(0,0,0,0.05)';
                }

                context.fill();
                context.stroke();
            });

            //smallSphere
            faces2.forEach(function(d) {
                d.polygon[0] = projection2(d[0]);
                d.polygon[1] = projection2(d[1]);
                d.polygon[2] = projection2(d[2]);

                if (icon_count % (faces2.length / company_length - faces2.length / company_length % 1) === 0){
                    pointsArray.push({projection_val: d.polygon[0], initial:d[0], main_projection: projection(d[0]), id: company_id});
                    company_id++;
                } else {
                    if(d.visible = d.polygon.area() > 0){
                        context.fillStyle = '#000';
                        context.fillRect(d.polygon[0][0]-2.5, d.polygon[0][1]-2.5, 5, 5);
                    }
                }
                icon_count++;
                if (isPlusShow){
                  for (var element in pointsArray){
                      if('projection_val' in pointsArray[element]){
                          if(hoverStatus === false){
                              context.fillStyle = rectColors[element % 4];
                              context.fillRect(pointsArray[element].projection_val[0]-icon_width/2 * factor, pointsArray[element].projection_val[1]-icon_width/2 * factor, icon_width * factor, icon_width * factor);
                              context.fillStyle = "#f5f5f5";
                              context.fillRect(pointsArray[element].projection_val[0], pointsArray[element].projection_val[1]-(plus_width-1)/2 * factor, 1 * factor, plus_width * factor);
                              context.fillRect(pointsArray[element].projection_val[0]-(plus_width-1)/2 * factor, pointsArray[element].projection_val[1], plus_width * factor, 1 * factor);

                              if($(icon_hover).hasClass('active')){
                                  $(icon_hover).removeClass('active').css("display","none");
                              }
                          }
                      }
                  }
                }

                if(hoverStatus === true){
                    // hoverX = hoverX - 19 * factor;
                    // hoverY = hoverY - 19 * factor;

                    // context.fillStyle = '#0c0c0c';
                    // context.fillRect(hoverX, hoverY, 38 * factor, 38 * factor);
                    // context.fillStyle = '#fff';
                    // context.fillRect(hoverX+13 * factor, hoverY+19 * factor, 10 * factor, 1 * factor);
                    // context.fillRect(hoverX+23 * factor, hoverY+18 * factor, 2 * factor, 3 * factor);
                    // context.fillRect(hoverX+23 * factor, hoverY+17 * factor, 1 * factor, 1 * factor);
                    // context.fillRect(hoverX+25 * factor, hoverY+19 * factor, 1 * factor, 1 * factor);
                    // context.fillRect(hoverX+23 * factor, hoverY+21 * factor, 1 * factor, 1 * factor);

                    // hoverX = hoverX + 19 * factor;
                    // hoverY = hoverY + 19 * factor;

                    if(hoverShow === true){
                        var width_factor = parseInt(Ember.$('canvas.scene').css('width'), 10)/canvas_initial_width;
                        var height_factor = parseInt(Ember.$('canvas.scene').css('height'), 10)/canvas_initial_width;
                        $('.sphereHoverContainer').css({
                            'margin-left':'-'+ (width * width_factor) / 2 +'px',
                            'width':width * width_factor +'px',
                        });

                        $('.plus').addClass('active').css({
                            'display':'block',
                            'top': hoverY / factor * width_factor - icon_width/2 + amendment + "px",
                            'left': hoverX / factor * height_factor - icon_width/2 + "px",
                            'transform':'scale('+width_factor+','+height_factor+')'
                        });

                        hoverShow = false;
                    }
                }

                context.beginPath(d.polygon[0]);
                if (d.visible = d.polygon.area() > 0) {
                    context.lineWidth = 1.2;
                    context.strokeStyle = "#57524b";
                } else {
                    context.lineWidth = 0.8;
                    context.strokeStyle = "#a9a49f";
                }
                context.fillStyle = d.fill;

                drawTriangle(d.polygon);

                if (d.fill === "rgba(0,0,0,0.1)" && d.visible) {
                    d.fill = 'rgba(0,0,0,0.1)';
                }
                if (d.fill === "rgba(0,0,0,0.1)" && !d.visible) {
                    d.fill = 'rgba(0,0,0,0.05)';
                }

                context.fill();
                context.stroke();
            });
        }

        /*function getMousePos(canvas, evt) {
            var rect = canvas.getBoundingClientRect();
            return {
                x: evt.clientX - rect.left,
                y: evt.clientY - rect.top
            };
        }*/

        var text_hover = $('.sphereCompany').textillate({
            in: {effect: 'fadeInRight', delay: 0.1},
            out: {effect: 'fadeOutLeft', delay: 0.1},
            autoStart: false,
            type: 'word'
        });
        $('.sphereCompanyLink').html("Ваши задачи, наши решения");

        text_hover.textillate('start');

        var button_status = false;
        var prev_text = '';
        var text_timeOut;

        $(window).resize(function(){

        });

        var canvas_initial_width = parseInt(Ember.$('canvas.scene').css('width'), 10);
        var canvas_initial_height = parseInt(Ember.$('canvas.scene').css('height'), 10);
        $('canvas.scene').mousemove(function(event){
            hoverStatus = false;
            button_status = false;
            var x = event.pageX - canvas.offsetLeft,
                y = event.pageY - canvas.offsetTop;

            if(canvas_initial_width !== parseInt(Ember.$('canvas.scene').css('width'), 10)){
                var width_factor = parseInt(Ember.$('canvas.scene').css('width'), 10)/canvas_initial_width;
                var height_factor = parseInt(Ember.$('canvas.scene').css('height'), 10)/canvas_initial_width;
                x = x / width_factor;
                y = y / height_factor;
            }
            pointsArray.forEach(function(element) {
                if (x > element.projection_val[0]/factor-icon_width/2 &&
                    x < element.projection_val[0]/factor+icon_width/2 &&
                    y > element.projection_val[1]/factor-icon_width/4 &&
                    y < element.projection_val[1]/factor+icon_width) {
                    Ember.$('.plus').attr('href', '/about/'+(element.id+1));
                    if(companies[element.id+1] !== prev_text){
                        text_hover.textillate('out');

                        clearTimeout(text_timeOut);
                        text_timeOut = setTimeout(function() {
                            $('.sphereCompanyLink').html(companies[element.id+1]);
                            text_hover.textillate('start');
                            prev_text = companies[element.id+1];
                        }, 500);
                    }
                    button_status = true;
                    hoverX = element.projection_val[0];
                    hoverY = element.projection_val[1];
                    hoverStatus = true;
                    hoverShow = true;
                }
            });

            if($('.sphereCompanyLink').html() !== "Ваши задачи, наши решения" && !button_status){
                text_hover.textillate('out');

                clearTimeout(text_timeOut);
                text_timeOut = setTimeout(function() {
                    $('.sphereCompanyLink').html("Ваши задачи, наши решения");
                    text_hover.textillate('start');
                    prev_text = '';
                }, 500);
            }
        });

        // canvas.addEventListener("click", function(event){
        //     redraw();
        //     var x = event.pageX - canvas.offsetLeft,
        //         y = event.pageY - canvas.offsetTop;
        //     pointsArray.forEach(function(element) {
        //         if (x > element.projection_val[0]/factor-19 && x < element.projection_val[0]/factor+19 && y > element.projection_val[1]/factor-9.5 && y < element.projection_val[1]/factor+28.5) {
        //             location.href = "/about/"+(element.id+1);
        //         }
        //     });
        // }, false);

        function drawTriangle(triangle) {
            context.moveTo(triangle[0][0], triangle[0][1]);
            context.lineTo(triangle[1][0], triangle[1][1]);
            context.lineTo(triangle[2][0], triangle[2][1]);
            context.closePath();
        }

        function geodesic() {
            faces = d3.geodesic.polygons(3).map(function(d) {
                d = d.coordinates[0];
                d.pop(); // use an open polygon
                d.fill = randexec(["rgba(0,0,0,0.1)", "rgba(0,0,0,0)"], [3, 97]);
                d.polygon = d3.geom.polygon(d.map(projection));
                return d;
            });
            faces2 = d3.geodesic.polygons(1).map(function(d) {
                d = d.coordinates[0];
                d.pop(); // use an open polygon
                d.fill = randexec(["rgba(0,0,0,0.1)", "rgba(0,0,0,0)"], [20, 80]);
                d.polygon = d3.geom.polygon(d.map(projection));
                return d;
            });
            redraw();
        }

        //probability
        function randexec(what, prob) {
            var ar = [];
            var i, sum = 0;
            for (i = 0; i < prob.length - 1; i++) {
                sum += (prob[i] / 100.0);
                ar[i] = sum;
            }
            var r = Math.random(); // returns [0,1]
            for (i = 0; i < ar.length && r >= ar[i]; i++) {}
            return what[i];
        }

        //smooth stop
        function mouseover() {
            stop = true;
            start = false;
            run = false;
        }

        //smooth run
        function mouseout() {
            stop = false;
            start = true;
            run = false;
        }

        //on icon over
        $(icon_hover).hover(
          function() {
            mouseover();
          }, function() {
            mouseout();
          }
        );
        // $(icon_hover).click(function(e){
        //     e.preventDefault();
        //     var x = event.pageX - canvas.offsetLeft,
        //         y = event.pageY - canvas.offsetTop;
        //     pointsArray.forEach(function(element) {
        //         if (x > element.projection_val[0]/factor-19 && x < element.projection_val[0]/factor+19 && y > element.projection_val[1]/factor-9.5 && y < element.projection_val[1]/factor+28.5) {
        //             location.href = "/about/"+(element.id+1);
        //         }
        //     });
        // });

        var dragBehaviour = d3.behavior.drag()
            .on('drag', function() {
                var dx = d3.event.dx;
                var dy = d3.event.dy;

                var rotation = projection.rotate();
                var radius = projection.scale();
                var scale = d3.scale.linear()
                    .domain([-1 * radius, radius])
                    .range([-90, 90]);
                var degX = scale(dx);
                var degY = scale(dy);
                rotation[0] -= degX;
                rotation[1] += degY;
                projection.rotate(rotation);
                projection2.rotate(rotation);
                redraw();
                hoverStatus = false;
                hoverShow = false;
            });

        // attach event listeners
        var run = true,
            start = false,
            stop = false;
        d3.select(canvas)
        .on("mouseover", mouseover)
        .on("mouseout", mouseout)
        .call(dragBehaviour);
        //main loop
        d3.timer(function() {
            let rotation = projection.rotate();
            if (run) {
                projection.rotate([rotation[0]+velocity[0], rotation[1]+velocity[1]]);
                projection2.rotate([rotation[0]+velocity[0], rotation[1]+velocity[1]]);
                var pt = parseInt($(canvas).css('padding-left'), 10);
                isPlusShow = pt > 0 ? false : true
                redraw();
            }
            if (start) {
                velocity[0] = velocity[0]+division[0]; if (velocity[0]>0.04) {velocity[0]=0.04;start=false;run=true;}
                velocity[1] = velocity[0]+division[1]; if (velocity[1]>0.07) {velocity[1]=0.07;}
                projection.rotate([rotation[0]+velocity[0], rotation[1]+velocity[1]]);
                projection2.rotate([rotation[0]+velocity[0], rotation[1]+velocity[1]]);
                redraw();
            }
            if (stop) {
                velocity[0] = velocity[0]-division[0]; if (velocity[0]<0) {velocity[0]=0;}
                velocity[1] = velocity[0]-division[1]; if (velocity[1]<0) {velocity[1]=0;}
                projection.rotate([rotation[0]+velocity[0], rotation[1]+velocity[1]]);
                projection2.rotate([rotation[0]+velocity[0], rotation[1]+velocity[1]]);
                redraw();
            }
        });
    }
});
