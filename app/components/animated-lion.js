import Ember from 'ember';

export default Ember.Component.extend({
	didInsertElement: function() {
		var colors = ['#e03030', '#7318a7', '#223d9f', '#50aeda', '#00a253', '#fbe91c', '#e03030'];
		var els = $('#lion polygon');
		var elsArray = $.map(els, function(value, index) {
			return [value];
		});
		elsArray.reverse();
		var iterationCount = 0;

		function generateColors() {
			console.log(iterationCount);
			var rainbow = new Rainbow();
			rainbow.setSpectrum(colors[iterationCount], colors[iterationCount+1]);
			rainbow.setNumberRange(0, els.length);
			$.each(elsArray, function( i, l ){
				$(l).attr('fill', '#' + rainbow.colourAt(i));
			});
			iterationCount++;
		}
		try {
			// generate colors when page just load
			generateColors();
			// generate colors to make gradient effect
			setInterval(function(){
				if (iterationCount < colors.length-1) {
					generateColors();
				} else {
					console.log('loop ended');
					iterationCount = 0;
				}
			}, 25000)
			// we have a little problem, animation don`t play reverse
		} catch (err) {
			console.log(err);
		}
}
});
