import DS from 'ember-data';

export default DS.Model.extend({
	name: DS.attr(),
	phone: DS.attr(),
	email: DS.attr(),
	company: DS.attr(),
	text: DS.attr(),
	created_at: DS.attr(),
	idContact: DS.attr()
	// sendData: function(_success, _error) {
	// 	console.log(JSON.stringify({contact: this.toJSON()}));
	// 	Ember.$.ajax({
	// 		type: 'POST',
	// 		dataType: "json",
	// 		contentType: "application/json",
	// 		url: 'http://vivaki.apexapps.kz/api/contacts',
	// 		data: JSON.stringify({
	// 			contact: this.toJSON()
	// 		}),
	// 		success: function (data) {
	// 			console.log(data);
	// 		},
	// 		error: function(resp) {
	// 			console.log(resp);
	// 		}
	// 	});
	// }
});
