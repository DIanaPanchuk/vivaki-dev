import DS from 'ember-data';

export default DS.Model.extend({
	name: DS.attr(),
	description: DS.attr(),
	image: DS.attr(),
	svg_map: DS.attr(),
	status: DS.attr(),
	number: DS.attr(),
	companies: DS.attr()
});
