import DS from 'ember-data';

export default DS.Model.extend({
	job_id: DS.attr(''),
	file: DS.attr('file')
});
