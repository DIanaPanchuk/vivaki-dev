import DS from 'ember-data';

export default DS.Model.extend({
	name: DS.attr(),
	city: DS.attr(),
	country: DS.attr(),
	description: DS.attr(),
	mission: DS.attr(),
	deals: DS.attr(),
	add1: DS.attr(),
	add2: DS.attr(),
	seo_title: DS.attr(),
	seo_keys: DS.attr(),
	seo_desc: DS.attr(),
	seo_text: DS.attr(),
	image: DS.attr(),
	svg_logo_grey: DS.attr(),
	svg_logo_color: DS.attr(),
	status: DS.attr(),
	number: DS.attr(),
	services: DS.attr(),
	dateFound: DS.attr(),
	clients: DS.attr()
});
