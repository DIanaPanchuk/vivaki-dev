import DS from 'ember-data';

export default DS.Model.extend({
	name: DS.attr(),
	phone: DS.attr(),
	time_at: DS.attr(),
	company: DS.attr(),
	text: DS.attr(),
	created_at: DS.attr()
});
