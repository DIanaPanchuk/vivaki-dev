import DS from 'ember-data';

export default DS.Model.extend({
	name: DS.attr(),
	city: DS.attr(),
	country: DS.attr(),
	experience: DS.attr(),
	salary: DS.attr(),
	duties: DS.attr(),
	requirements: DS.attr(),
	conditions: DS.attr(),
	messages: DS.attr(),
	seo_title: DS.attr(),
	seo_keys: DS.attr(),
	seo_desc: DS.attr(),
	seo_text: DS.attr(),
	status: DS.attr(),
	created_at: DS.attr()
});
