import Ember from 'ember';

export default Ember.Controller.extend({
    showLogo: function(){
        var routes_array = ['social', 'success', 'feedback', 'call-me', 'map', 'companies-list'],
            currentPath = this.get('currentPath'),
            showLogo = true;

        routes_array.forEach(function(item){
            if(currentPath===item) {
                showLogo = false;
            }
        });
        return showLogo;
    }.property('currentPath'),
    showSphere: function() {
        var currentPath = this.get('currentPath');
        if (currentPath == "main" || currentPath == "about") {
            return true;
        } else {
            return false;
        }
    }.property('currentPath')
});