import Ember from 'ember';

export default Ember.Controller.extend({
	texts: function() {
		var texts_temp = this.get('model.texts').get('content');
		var texts = {};
		$.each(texts_temp, function(){
			texts[this._data.name]=this._data.value;
		});
		return texts;
	}.property(),
});
