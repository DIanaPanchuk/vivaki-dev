import Ember from 'ember';

export default Ember.Controller.extend({
	sortProperties: ['number:asc'],
	sortedCompanies: Ember.computed('content', function(){
		var companies = this.get('companies');
		return companies.sortBy('number');
	})
});
