import Ember from 'ember';

export default Ember.Controller.extend({
	actions: {
		closeModal: function() {
			// Ember.$('.main').toggleClass('invisible');
			// Ember.$('.sphere-wrap').toggleClass('hidden');
			Ember.$('.main').addClass('slideUp');
			Ember.$('.sphere-wrap').addClass('slideUp');

			Ember.$('.liquid-container').removeClass('toFront');

			Ember.$('.sphere, .sphereCompany').removeClass('sphereHide');

			Ember.$('body').removeClass('ovfHidden');

			var self  = this;
			var cookie = this.get('cookie');

			cookie.setCookie('instructions', 'false', { expires: 500 })
				.then(function() {
					console.log('cookie saved');
				});
		}
	}
});
