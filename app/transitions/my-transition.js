import { animate, stop } from "liquid-fire";

export default function(opts={}) {
  var firstStep = animate(this.oldElement, {scale: [0.2, 1]}, opts);
  return firstStep.then(() => {
      return animate(this.newElement, {scale: [1, 0.2]}, opts);
  });
}