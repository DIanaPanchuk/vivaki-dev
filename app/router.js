import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
  location: config.locationType
});
Router.reopen({
  addClassByRoute: function() {
    var current = this.currentRouteName + '-view';
    $("body").removeAttr('class');
    $("body").addClass('ember-application');
    $("body").addClass(current);

    if (this.currentRouteName == "feedback" || this.currentRouteName == "call-me" || this.currentRouteName == "social") {
      Ember.$('.hamburger').addClass('opened');
    }
  }.on('didTransition'),
 closeMenu: function() {
    Ember.$('.hamburger').removeClass('opened');
    // Ember.$('.liquid-container').removeClass('hidden');
    Ember.$('#menu').removeClass('visible');
    Ember.$('#logo').removeClass('invisible');

    Ember.$('.liquid-container').removeClass('menuOpened');
    Ember.$('#menu').removeClass('open');
    $('body > div').removeClass('menuOpened');

    return;
  }.on('didTransition'),
  sphereMoving: function(){
    setTimeout(function(){Ember.$('.main-view .sphere').addClass('showSphere');}, 500);
    if (this.currentRouteName == 'about') {
        var totalWidth = $('.sphere').width();
        var sphereWidth = $('.anim.r').height() - $('.bottomMenu').outerHeight() - $('.anim.t').outerHeight();
        var padding = (((totalWidth-sphereWidth))+(sphereWidth/2));
        $('.sphere canvas').css({'padding-left': padding});
    }
  }.on('didTransition')
});

Router.map(function() {
  this.route('main', {path: '/'});
  this.route('about', {path: '/about'});
  this.route('company', {path: '/about/:company_id'});
  this.route('vacancies', {});
  this.route('contact-info', {});
  this.route('companies-list', {});
  this.route('map-of-presence', {});
  this.route('feedback', {});
  this.route('call-me', {});
  this.route('map', {});
  this.route('social', {});
  this.route('404', {path: '/*path'});
  this.route('success');
  this.route('map-of-presence-desc');
  this.route('nav');
});

export default Router;
