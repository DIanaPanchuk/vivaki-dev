import ApplicationAdapter from './application';
import FormDataAdapterMixin from 'vivaki/mixins/form-data-adapter';

export default ApplicationAdapter.extend(FormDataAdapterMixin, {
});
