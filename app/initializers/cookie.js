import Cookie from '../lib/cookie';

export default {
	name: 'cookie',
		initialize: function(app) {
		app.register('cookie:main', Cookie);
	}
};
