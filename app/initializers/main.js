export function initialize() {
	let application = arguments[1] || arguments[0];
	application.inject('controller', 'cookie', 'cookie:main');
}

export default {
	name: 'main-initializer',
	after: ['cookie'],
	initialize: initialize
}