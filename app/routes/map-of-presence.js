import Ember from 'ember';

export default Ember.Route.extend({
	// model: function(){
	//   return {
	//     'coordinates': [
	//       {lat: 10, lng: 0},
	//       {lat: 20, lng: 10},
	//       {lat: 25, lng: -10},
	//       {lat: 10, lng: 20},
	//     ]
	//   };
	// }
	model: function() {
		// return this.store.findAll('country');
		return Ember.RSVP.hash({
			country: this.store.findAll('country'),
			coordinates: this.store.findAll('coordinate')
		});
	},
	setupController: function(controller, model) {

		controller.set('countries', model.country);

		var coordinates_data = model.coordinates.get('content');
		var coordinates = [];
		$.each(coordinates_data, function(){
			coordinates.push({lat: this._data.lat, lng: this._data.lng});
		});
		controller.set('coordinates', coordinates);

		//get texts
		var texts_temp = this.modelFor('application').texts.get('content');
		var texts = {};
		$.each(texts_temp, function(){
			texts[this._data.name]=this._data.value;
		});
		controller.set('texts', texts);
	}
});
