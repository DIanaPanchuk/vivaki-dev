import Ember from 'ember';

export default Ember.Route.extend({
	model: function() {
		return this.store.peekAll('company');
	},
	setupController: function(controller, model) {
		controller.set('companies', model);

		//get texts
		var texts_temp = this.modelFor('application').texts.get('content');
		var texts = {};
		$.each(texts_temp, function(){
			texts[this._data.name]=this._data.value;
		});
		controller.set('texts', texts);
	}
});
