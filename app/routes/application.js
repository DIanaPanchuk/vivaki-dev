import Ember from 'ember';

export default Ember.Route.extend({
	model: function() {
		return Ember.RSVP.hash({
			company: this.store.findAll('company'),
			texts: this.store.findAll('text')
		});
	},
	setupController: function(controller, model) {
		// var texts_temp = model.get('content');
		var texts_temp = model.texts.get('content');
		var texts = {};
		$.each(texts_temp, function(){
			texts[this._data.name]=this._data.value;
		});
		controller.set('texts', texts);

		// get companiesr list
		var companies_data = model.company.get('content');
		var companies = {};
		$.each(companies_data, function(){
			companies[this.id]=this._data.name;
		});
		controller.set('company', companies);

	},
	actions: {
		changeLang: function(lang) {
			var lang = lang;
			$.ajax({
				type: 'GET',
				dataType: "json",
				url: Vivaki['api']['host'] + 'api/set_language/' + lang,
				cache: false,
				crossDomain: true,
				xhrFields: {
					withCredentials: true
				},
				success: function (response) {
					console.log(response);
					location.reload();
				},
				error: function(response) {
					console.log(response);
				}
			});
		}
	}
});
