import Ember from 'ember';

export default Ember.Route.extend({
	model: function() {
		return this.store.peekAll('company');
	},
	setupController: function(controller, model) {
		// get companiesr list
		var companies_data = model.get('content');
		var companies = {};
		$.each(companies_data, function(){
			companies[this.id]=this._data.name;
		});
		controller.set('company', companies);

		// get texts
		var texts_temp = this.modelFor('application').texts.get('content');
		var texts = {};
		$.each(texts_temp, function(){
			texts[this._data.name]=this._data.value;
		});
		controller.set('texts', texts);


		var cookie = controller.get('cookie');
		var token = cookie.getCookie('instructions');

		if (!token || token === "undefine") {
			token = "show";
		}
		controller.set('getCookie', token);
	}
});
