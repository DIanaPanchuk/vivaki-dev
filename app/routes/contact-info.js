import Ember from 'ember';
import fileUpload from 'vivaki/mixins/file-upload';

export default Ember.Route.extend(fileUpload, {
	model: function() {
		return Ember.Object.create({
			name: "",
			phone: "",
			file: "",
			company: "",
			text: ""
		});
	},
	setupController: function(controller, model) {
		controller.set('contact', model);
		//get texts
		var texts_temp = this.modelFor('application').texts.get('content');
		var texts = {};
		$.each(texts_temp, function(){
			texts[this._data.name]=this._data.value;
		});
		controller.set('texts', texts);
	},
	actions: {
		sendContacts: function(contact) {
			var self = this;
			var data = {
				name: contact.name,
				phone: contact.phone,
				email: contact.email,
				company: contact.company,
				text: contact.text
			};

			this.store.createRecord('contact', data)
				.save()
				.then(
					function(resp) {
						// debugger
						self.fileUpload(
							[{name: 'file', key: 'file'}],
							{url: "upload/contact/" + resp.get('idContact')},
							function(resp){
								console.log(resp);
							}, function(resp) {
								console.log(resp);
							});
						self.transitionTo("success");
					}, function(resp) {
						console.log(resp);
					}
				);
		}
	}
});
