import Ember from 'ember';

export default Ember.Route.extend({
	model: function() {
		return Ember.Object.create({
			name: "",
			phone: "",
			email: "",
			time_at: "",
			company: "",
			text: ""
		});
	},
	setupController: function(controller, model) {
		controller.set('call', model);

		// get texts
		var texts_temp = this.modelFor('application').texts.get('content');
		var texts = {};
		$.each(texts_temp, function(){
			texts[this._data.name]=this._data.value;
		});
		controller.set('texts', texts);

		var preferredTimeList = [
			{
				time: 'does not matter',
				text: texts.preferred_time_dontmatter
			},{
				time: 'morning',
				text: texts.preferred_time_morning
			},{
				time: 'afternoon',
				text: texts.preferred_time_afternoon
			}
		];
		controller.set('preferredTimeList', preferredTimeList);
	},
	actions: {
		preferredTimeSelected: function(preferredTime){
			console.log(preferredTime);
		},
		sendCall: function(call) {
			var self = this;
			if (call.time_at.text) {
				var preferredTime = call.time_at.text;
			} else {
				var preferredTime = self.controllerFor('call-me').get("texts").preferred_time_dontmatter;
			}
			var data = {
				name: call.name,
				phone: call.phone,
				company: call.company,
				time_at: preferredTime,
				text: call.text
			};
			this.store.createRecord('call', data)
				.save()
				.then(
					function(resp) {
						self.transitionTo("success");
					}, function(resp) {
						console.log(resp);
					}
				);
		}
	}
});
