import Ember from 'ember';
import fileUpload from 'vivaki/mixins/file-upload';

export default Ember.Route.extend(fileUpload, {
	model: function() {
		return Ember.RSVP.hash({
			jobs: this.store.findAll('job'),
			texts: this.modelFor('application').texts
		});
	},
	actions: {
		sendFile: function(file) {
			var self = this;
			var job_id = file.id,
				fileName = 'file' + job_id;

			this.fileUpload([
				{name: fileName, key: 'file'}],
				{url: "upload/job_message/" + job_id},
				function(resp){
					console.log(resp);
					self.transitionTo("success");
				}, function(resp) {
					console.log(resp);
				});
		}
	}
});
