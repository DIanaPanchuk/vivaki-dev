import Ember from 'ember';
import fileUpload from 'vivaki/mixins/file-upload';

export default Ember.Route.extend(fileUpload, {
	model: function(params){
		return Ember.RSVP.hash({
			company: this.store.peekRecord('company', params.company_id),
			companies: this.store.peekAll('company')
		});
	},
	setupController: function(controller, model) {

		var company = model.company;
		var companies = model.companies;

		controller.set('company', company);
		controller.set('companies', companies);

		// for feedback form
		controller.set('contact', Ember.Object.create({
			name: "",
			phone: "",
			file: "",
			company: "",
			text: ""
		}));

		//get texts
		var texts_temp = this.modelFor('application').texts.get('content');
		var texts = {};
		$.each(texts_temp, function(){
			texts[this._data.name]=this._data.value;
		});
		controller.set('texts', texts);

		var companies_data = [];
		Ember.$.each(companies.get('content'), function(){
			companies_data.push(this.id);
		});

		var currentIndex = companies_data.indexOf(company.id);

		var next = companies_data[currentIndex + 1];
		var prev = companies_data[currentIndex - 1];

		controller.set('nextCompany', next);
		controller.set('previousCompany', prev);


	},
	actions: {
		sendContacts: function(contact) {
			var self = this;
			var data = {
				name: contact.name,
				phone: contact.phone,
				email: contact.email,
				company: contact.company,
				text: contact.text
			};
			// console.log(data);
			this.store.createRecord('contact', data)
				.save()
				.then(
					function(resp) {
						self.fileUpload([{name: 'file', key: 'file'}], {url: "upload/contact/" + resp.get('idContact')},
							function(resp){
								console.log(resp);
							}, function(resp) {
								console.log(resp);
							});
						self.transitionTo("success");
					}, function(resp) {
						console.log(resp);
					}
				);
		}
	}
});
