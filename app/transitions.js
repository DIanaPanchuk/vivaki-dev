export default function(){
    // this.transition(
    //     this.fromRoute('main'),
    //     this.toRoute('map-of-presence'),
    //     this.use('customToLeft', { duration: 1000 }),
    //     this.reverse('customToRight', { duration: 1000 })
    // );
    // this.transition(
    //     this.fromRoute('map-of-presence'),
    //     this.toRoute('main'),
    //     this.use('customToRight', { duration: 1000 }),
    //     this.reverse('customToLeft', { duration: 1000 })
    // );
    this.transition(
        this.fromRoute('main'),
        this.toRoute('about'),
        this.use('customToRight', { duration: 1000 }),
        this.reverse('customToLeft', { duration: 1000 })
    );
    this.transition(
        this.toRoute('map'),
        this.use('toUp', { duration: 1000 }),
        this.reverse('toDown', { duration: 1000 })
    );
    this.transition(
        this.toRoute('success'),
        this.use('toUp', { duration: 1000 }),
        this.reverse('toUp', { duration: 1000 })
    );
}