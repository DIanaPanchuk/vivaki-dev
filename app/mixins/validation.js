import Ember from 'ember';

export default Ember.Mixin.create({
	validate: function(){
		var texts = this.texts;

		Ember.$('.form-validate').validate({
			rules: {
				name: {
					required: true,
					minlength: 2
				},
				phone: {
					required: true,
					digits: true
				},
				email: {
					required: true,
					email: true
				},
				company: {
					required: true,
				},
				time_at: {
					required: false
				},
				text: {
					required: true,
					minlength: 10
				},
				file: {
					required: false
				}
			},
			messages: {
				name: {
					required: texts.validate_name,
					minlength: texts.validate_name_length
				},
				phone: {
					required: texts.validate_phone,
					digits: texts.validate_phone_length
				},
				email: {
					required: texts.validate_email,
					email: texts.validate_email_valid
				},
				company: texts.validate_company,
				time_at: texts.validate_time_at,
				text: {
					required: texts.validate_text,
					minlength: texts.validate_text_length
				},
				file: {
					required: texts.validate_file
				}
			}
		});
	}
});
