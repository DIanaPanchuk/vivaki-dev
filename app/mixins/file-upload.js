import Ember from 'ember';

export default Ember.Mixin.create({
	fileUpload: function(fileArray, ajax, _success, _error) {
		var formData = new FormData(),
				fileLength = 0;

		fileArray.forEach(function(item) {
			var file = Ember.$('[name="' + item.name + '"')[0];
			if(file.files.length) {
				formData.append([item.key], file.files[0]);
				fileLength++;
			}
		});

		if(fileLength) {
			Ember.$.ajax({
				url: Vivaki['api']['host'] + 'api/' + ajax.url,
				data: formData,
				cache: false,
				contentType: false,
				processData: false,
				type: 'POST',
				success: function(resp){
					_success(resp);
				},
				error: function(resp) {
					_error(resp);
				}
			});
		}
	}
});