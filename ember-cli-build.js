/* global require, module */
var EmberApp = require('ember-cli/lib/broccoli/ember-app');
var mergeTrees = require('broccoli-merge-trees');
var pickFiles = require('broccoli-static-compiler');
var nib = require('nib');

module.exports = function(defaults) {
  var app = new EmberApp(defaults, {
    tests: false,
    // Add options here
    stylusOptions: {
      use: [nib()],
      compress: true,
      includePaths: [
        // 'bower_components/foundation/styl'
      ]
    }
  });

  // Use `app.import` to add additional libraries to the generated
  // output files.
  //
  // If you need to use different assets in different
  // environments, specify an object as the first parameter. That
  // object's keys should be the environment name and the values
  // should be the asset to use in that environment.
  //
  // If the library that you are including contains AMD or ES6
  // modules that you would like to import into your application
  // please specify an object with the list of modules as keys
  // along with the exports of each module as its value.

  var fonts = pickFiles('vendor/fonts/intro',{
      srcDir: '/',
      files: ['**/*'],
      destDir: '/assets/fonts/intro'
  });

  var images = pickFiles('vendor/i',{
      srcDir: '/',
      files: ['**/*'],
      destDir: '/assets/i'
  });

  var fav = pickFiles('vendor/fav',{
      srcDir: '/',
      files: ['**/*'],
      destDir: '/assets/i/fav'
  });

  //animate number
  app.import('bower_components/jquery-animateNumber/jquery.animateNumber.js');
  //ScrollMagic
  app.import('bower_components/gsap/src/uncompressed/TweenMax.js');
  app.import('bower_components/gsap/src/uncompressed/plugins/ScrollToPlugin.js');
  app.import('bower_components/scrollmagic/scrollmagic/uncompressed/ScrollMagic.js');
  app.import('bower_components/scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap.js');
  // app.import('bower_components/scrollmagic/scrollmagic/uncompressed/plugins/animation.velocity.js');
  app.import('bower_components/scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators.js');
  //owl carousel
  app.import('bower_components/owl.carousel/dist/assets/owl.carousel.min.css');
  app.import('bower_components/owl.carousel/dist/assets/owl.theme.default.min.css');
  app.import('bower_components/owl.carousel/dist/owl.carousel.min.js');
  //d3.js
  app.import('bower_components/d3/d3.js');
  app.import('bower_components/particles.js/particles.min.js');
  app.import('vendor/particles-config.json', {
    destDir: 'assets'
  });
  // validation
  app.import('bower_components/jquery-validation/dist/jquery.validate.js');
  //gradient-generator
  app.import('vendor/rainbowvis.js');
  //  Textillate.js
  app.import('vendor/jquery.lettering.js');
  app.import('vendor/jquery.textillate.js');
  // cookies
  app.import('vendor/jquery.cookie.js');
  // nice scroll
  app.import('vendor/jquery.nicescroll.js');

  return mergeTrees([app.toTree(), fonts, images, fav]);
};
