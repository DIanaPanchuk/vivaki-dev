import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('icosahedron-sphere', 'Integration | Component | icosahedron sphere', {
  integration: true
});

test('it renders', function(assert) {
  assert.expect(2);

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{icosahedron-sphere}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#icosahedron-sphere}}
      template block text
    {{/icosahedron-sphere}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
